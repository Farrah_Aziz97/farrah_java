package com.example.demo.basic;

public class luas {
    private Integer panjang;
    private Integer lebar;

    //untuk set get
    public luas()
    {

    }

    public luas(Integer panjang, Integer lebar)
    {
        this.panjang = panjang;
        this.lebar = lebar;
    }

    public  Integer hitungluas()
    {
        Integer luas = panjang * lebar;
        return luas;
    }

    public Integer getPanjang() {
        return panjang;
    }

    public void setPanjang(Integer panjang) {
        this.panjang = panjang;
    }

    public Integer getLebar() {
        return lebar;
    }

    public void setLebar(Integer lebar) {
        this.lebar = lebar;
    }

}

