package com.example.demo.basic;
//warna ungu = variabel local
//putih variabel bebas
public class volume extends luas {
    private Integer sisi;
    private Integer panjang;
    private Integer lebar;
    private Integer tinggi;

    public volume(){

    }

    public volume(Integer panjang, Integer sisi, Integer lebar, Integer tinggi){
        //tanpa harus membuat private di extends luas
        //ctrl+click
        //super mencari konstruktor dari extend luas
        super(panjang,lebar);
     this.panjang = panjang;
     this.sisi = sisi;
     this.lebar = lebar;
     this.tinggi = tinggi;
    }

    public Integer hitungvolumebalok(){
        //cara manual
        //Integer volume= panjang * lebar * tinggi;
        //cara mudah
        Integer volume= hitungluas() * tinggi;
        return volume;
    }

    public Integer hitungvolumekubus(){
        Integer volume = sisi * sisi * sisi;
        return volume;
    }


    public Integer getLebar() {
        return lebar;
    }


    public void setLebar(Integer lebar) {this.lebar = lebar;}


    public Integer getPanjang(){
        return panjang;
    }


    public void setPanjnag(Integer panjang) {this.panjang = panjang;}

    public Integer getSisi(){
        return sisi;
    }

    public void setSisi(Integer sisi ) {this.sisi = sisi;}

    public Integer getTinggi(){
        return tinggi;
    }

    public void setTinggi(Integer tinggi) {this.tinggi = tinggi;}

}
