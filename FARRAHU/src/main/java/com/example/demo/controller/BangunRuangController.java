package com.example.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

//untuk menandakan bahwa ini contoller
@Controller
//ceritanya tuh controller(nama url)
@RequestMapping("/bangunruang")

public class BangunRuangController {
    //"/",""(rooting) untuk url (menggunakan / atau tidak)
    @RequestMapping(value = {"/",""}, method = RequestMethod.GET)
    public String index()
    {
        return "bangunruang/index";

        //manggil url localhost:9090/farrah/bangunruang
    }

    //membuat / baru
    @RequestMapping(value = {"/segitiga"}, method = RequestMethod.GET)
    public String segitiga()
    {
        return "bangunruang/segitiga";

        //manggil url localhost:9090/farrah/bangunruang
    }

    //end
}
